# blackjack
A simple blackjack game made in Vue.js by Sultan Iljasov and Edris Afzali.

# Getting started
In order to start a game, the user must enter a username to procceed. The game follows all the basic blackjack rules.

# API
The application uses [deckofcards](https://www.deckofcardsapi.com/) api to fetch a deck of cards. 
