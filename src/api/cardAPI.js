export async function fetchId()
{
    return new Promise(function(resolve, reject){
        fetch('https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1')
            .then(response => response.json())
            .then(json => {resolve(json.deck_id)})
            .catch(err => reject(new Error(err)))
    });
}

export async function fetchCard(id)
{
    return new Promise(function(resolve, reject) {
        fetch('https://deckofcardsapi.com/api/deck/'+ id +'/draw/?count=1')
            .then(response => response.json())
            .then(json => {resolve(json.cards[0])})
            .catch(err => reject(new Error(err)));
    });
}

export class Card
{
    constructor(code, value, image, show)
    {
        this.code = code;
        this.value = value;
        this.image = image;
        this.show = show;
    }
    getImage()
    {
        if (this.show) return this.image;
        else return 'http://www.simpleimageresizer.com/_uploads/photos/427a5996/d41fa9f18764188c1b13b18c8ac50a58_226x314.jpg';
    }
}

export function getTotal(player)
{
    let total = 0;
    for (const c of player)
    {
        if (c.value === 'KING' || c.value === 'QUEEN' || c.value === 'JACK')
            total += 10;
        else if (c.value === 'ACE')
        {
            total += (total > 10) ? 1 : 11;
        }
        else total += Number(c.value);
    }
    return total;
}

export function sleep(ms)
{
    return new Promise(resolve => setTimeout(() => resolve(), ms));
}