import VueRouter from 'vue-router'
import Start from "@/components/Start";
import Play from "@/components/Play";

const routes = [
    {
        path: '/',
        name: 'Start',
        component: Start
    },
    {
        path: '/play/:playerName',
        name: 'Play',
        component: Play
    }
]

const router = new VueRouter({mode:"history", routes })

export default router;